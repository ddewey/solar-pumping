//************************* OLED display stuff (for debugging):
#include <Wire.h> // only needed to check if the display is present
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

// **********
// constants that may need to be hard coded differently for different devices:

//const int VCC = 5000; // 5 volts in milliVolts for 5V device!!!
const int VCC = 3333; // 3.33 volts in milliVolts for 3.33V device!!!

//const long MAX_READ = 55670L; // for 5V device!!!: (R1 (9.89k) / R2 (0.977k) + 1) * 5.0V * 1000 * 1.001 (plus 0.1% fudge!!!)
const long MAX_READ = 36576L; // for 3.33V device #1 (red): (R1 (9.89k) / R2 (0.977k) + 1) * 3.33V * 1000 * 0.9875 (minus 1.25% fudge!!!)
// const long MAX_READ = 37139L; // for 3.33V device #2 (yellow): (R1 (9.89k) / R2 (0.977k) + 1) * 3.33V * 1000 * 1.0027 (plus 0.27% fudge!!!)

const bool USE_PROBE = true; // if there is a water level prob, otherwise only senses battery voltage
const bool LOW_WATER_LOW_POWER = false; // if there is a water level prob, also use it to ease off power as water level gets low
const unsigned long LOWER_LOW_TIMEOUT = 60000UL; // if no lower probe value in 1 minute, at low water level, something is wrong, stop
const unsigned long LOWER_LOW_TIMEOUT_MAX = 1800000UL; // when water level is higher, timeout adjusts upward to a max of 30 minutes
const unsigned long LOWER_LOW_RECOVERY = 300000UL; // recover from timeout stop in 5 minutes
const unsigned long MIN_PUMP_TIME = 180000UL; // pump for at least 3 minutes if water available, even if battery is low
const int MAX_CHARGE = 16800; // the highest possible battery charge we expect to see

// for 6 amp pump:
/* const int OVER_DISCHARGE = 15400; // stop pumping when adjusted battery is below or equal to this many mV
const int LOW_POWER_CHARGE = 15450; // pump at the lowest power level when adjusted battery gets below or equal to this many mV
const int MIN_START_CHARGE = 15600; // battery charge must be at least this many mV to start pumping
const int MIN_FULL_POWER_CHARGE = 16000; // battery must be at least this many mV to pump at full power level
const int PUMP_VOLTAGE_DROP = 1150; // approximation for how much the pump voltage drops when pump is on full power
const int PUMP_VOLTAGE = 12000; // the nominal mV to run the pump at full power
const int FULL_POWER = 10000; // let 100% be the full PUMP_VOLTAGE output power level (100 * 100)
const int XFULL_POWER =10100; // 101% power only used to get EMA to full power without waiting literally forever
const int LOW_POWER =   8000; // 80%: about the minimum power level needed to do useful pumping work (80 * 100)
const int KICK_POWER =  6000; // 60%: need at least this much power to get the motor turning initially
const int SHARP_STOP_FACTOR = -FULL_POWER; // if less than 0, stop more sharply toward the end, instead of easing into it */

// for 3.2 amp pump:
const int OVER_DISCHARGE = 15100; // stop pumping when adjusted battery is below or equal to this many mV
const int LOW_POWER_CHARGE = 15250; // pump at the lowest power level when adjusted battery gets below or equal to this many mV
const int MIN_START_CHARGE = 15400; // battery charge must be at least this many mV to start pumping
const int MIN_FULL_POWER_CHARGE = 16000; // battery must be at least this many mV to pump at full power level
const int PUMP_VOLTAGE_DROP = 400; // approximation for how much the pump voltage drops when pump is on full power
const int PUMP_VOLTAGE = 12000; // the nominal mV to run the pump at full power
const int FULL_POWER = 10000; // 100%; let 100% be the full PUMP_VOLTAGE output power level (100 * 100)
const int XFULL_POWER =10100; // 101% power only used to get EMA to full power without waiting literally forever
const int LOW_POWER =   8500; // 85%: about the minimum power level needed to do useful pumping work (85 * 100)
const int KICK_POWER =  6000; // 60%: need at least this much power to get the motor turning initially
const int SHARP_STOP_FACTOR = -FULL_POWER; // if less than 0, stop more sharply toward the end, instead of easing into it
// **********

// 0X3C+SA0 - 0x3C or 0x3D
const byte I2C_ADDRESS = 0x3C;

const String NL = String("\n"), BLANK = String("           ");

// depending on the font, a row of text is 11 chars long
#define LINE_LENGTH 11

SSD1306AsciiAvrI2c oled;

bool oledConnected() {
  static bool connected = false;
  static bool checkDone = false;
  if (!checkDone) {
    Wire.begin();
    Wire.beginTransmission(I2C_ADDRESS);
    byte error = Wire.endTransmission();
    checkDone = true;
    connected = error == 0;
  }
  return connected;
}

void oledSetup() {
  bool connected = oledConnected();
  if (connected) {
    oled.begin(&Adafruit128x64, I2C_ADDRESS);
    oled.setScrollMode(SCROLL_MODE_OFF);
    oled.setFont(System5x7);
    oled.set2X();
  }
}

String floatStr(String text, float n, int dec=2) {
  return text + ": " + String(n, dec);
}

String zeroPad(String str, int decs) {
  int len = str.length();
  if (len > decs) return str.substring(0, decs);
  if (len < decs) {
    String result = str;
    int diff = decs - len;
    for (int i=0; i<diff; i++) {
      result = "0" + result;
    }
    return result;
  }
  return str;
}

String intDecStr(String text, int n, int div=1000, int decs=3) {
  return text + ": " + String(n/div, DEC) + "." + zeroPad(String(n%div, DEC), decs);
}

String longStr(String text, long n) {
  return text + ": " + String(n, DEC);
}

String boolStr(bool state, String falseText="false", String trueText="true") {
  return state ? trueText : falseText;
}

String fixStrLen(String str) {
  int len = str.length();
  if (len > LINE_LENGTH) return str.substring(0, LINE_LENGTH);
  if (len < LINE_LENGTH) {
    String result = str;
    int diff = LINE_LENGTH - len;
    for (int i=0; i<diff; i++) {
      result += " ";
    }
    return result;
  }
  return str;
}

void printLines(String line1, String line2=BLANK, String line3=BLANK, String line4=BLANK) {
  oled.setCursor(0,0);
  oled.print(fixStrLen(line1) + NL + fixStrLen(line2) + NL + fixStrLen(line3) + NL + fixStrLen(line4));
}

// power a pin and analogRead multiple times, then unpower the pin
int powerRead(int powerPin, int sensePin, unsigned int readCount=100U) {
  int value=0, newValue=0;
  unsigned long sum = 0UL;
  unsigned int i;
  digitalWrite(powerPin, HIGH);
  // ignore the first readCount reads
  for (i=0U; i < readCount; i++) {
    analogRead(sensePin);
  }
  // average the remaining readCount reads
  for (i=0U; i < readCount; i++) {
    sum += analogRead(sensePin);
  }
  digitalWrite(powerPin, LOW);
  return sum / readCount;
}

//************************* probe stuff
const int PROBE_PIN = A0; // read probe off pin A0
const int PROBE_POWER_PIN = 4; // power the probe from pin 4
const int POT_PIN = A3; // use the pot to set the high water voltage level at which pumping should turn on
const int POT_POWER_PIN = 3; // power the high water pot with pin 3
const int STOP_VOLTAGE = 100; // stop pumping when the measured voltage gets to here or below (100 mV = 0.1 volt)
const int HIGH_PROBE_VOLTAGE = 1800; // the highest voltage that can typically be read from the water level probe
const int MIN_POT = STOP_VOLTAGE * 2; // in case the user sets the pot too low
const int MAX_POT_READ = 925; // 10k / (10k + 1k) * 1023 - 5 (minus 5 to make sure you can go all the way)
const int SLOW_RATIO = 80; // start slowing the pump if we get within this percent of STOP_VOLTAGE from pot setting

void probeSetup() {
  pinMode(PROBE_POWER_PIN, OUTPUT);
  digitalWrite(PROBE_POWER_PIN, LOW);
  pinMode(POT_POWER_PIN, OUTPUT);
  pinMode(POT_PIN, INPUT); //Optional
}

int getPotSetting() {
  if (!USE_PROBE) return MIN_POT;
  int read = powerRead(POT_POWER_PIN, POT_PIN);
  int mapped = map(read, 0, MAX_POT_READ, MIN_POT, HIGH_PROBE_VOLTAGE);
  return constrain(mapped, MIN_POT, HIGH_PROBE_VOLTAGE);
}

// returns milliVolts reading from the water level probe
int getProbeV() {
  if (!USE_PROBE) return VCC;
  int measured = powerRead(PROBE_POWER_PIN, PROBE_PIN);
  int mapped = map(measured, 0, 1023, 0, VCC);
  return VCC - mapped;
}

//************************* battery voltage stuff
const int RAW_PIN = A2; // read raw battery voltage off pin A2
const int RAW_MOSFET_PIN = 2; // turn on the mosfet for reading raw battery voltage via pin 2
const long EMA_MULT = 98L; // use 98 percent of the ema and 2 percent of the new value
const long BATT_EMA_MULT = 95L; // use 95 percent of the ema and 5 percent of the new value

void battSetup() {
  pinMode(RAW_MOSFET_PIN, OUTPUT);
  digitalWrite(RAW_MOSFET_PIN, LOW);
}

long updateEma(long ema, long newValue, long emaMult=EMA_MULT) {
  return (newValue * (100L-emaMult) + ema * emaMult) / 100L;
}

// returns milliVolts of our input power to determine charge state of battery
int getBattV() {
  int reading = powerRead(RAW_MOSFET_PIN, RAW_PIN);
  long mapped = map(reading, 0, 1023, 0, MAX_READ);
  long clipped = min(mapped, MAX_CHARGE);
  return clipped;
}

// returns the adjusted battV
int getBattVAdj(int battV, int power) {
  return battV + long(PUMP_VOLTAGE_DROP) * power / FULL_POWER;
}

// returns the ema of the batt milliVolts reading
int getBattEma(int battV, int ema) {
  return ema ? updateEma(ema, battV, BATT_EMA_MULT) : battV; // ema zero means we haven't initialized yet, initialize with the first reading
}

//************************* pump control stuff:
enum { TIMEOUT, STOPPED, PUMPING }; // states
const int PWM_PIN = 9;  //pin 9 has PWM funtion
const int STATE_LED_PIN = 13;      // the number of the LED pin that shows current state (LED is built in)
const unsigned long SLOW_LOOP_DELAY = 500UL; // delay to update stuff that only needs updating every 500 ms
const int MAX_ANALOG_WRITE = 255; // full on analog write (100% duty cycle)
const long MAX_PWM = 255000L; // full on PWM
const long FULL_PWM = 245000L; // If PWM exceeds this, might as well just output max PWM
const long START_TIME = 4000L; // this many ms to get motor up to full speed
const long STOP_TIME = 3000L; // this many ms to get motor down to a stop
const bool INVERT_PWM = true; // the mosfets will invert the PWM signal, so write the inverse if this is set true;
const unsigned long TIMEOUT_FLASH_PERIOD = 100UL; // flash the LED at this period ms if in timeout state
const unsigned long TIMEOUT_FLASH_DURATION = 25UL; // flash the LED for this duration ms if in timeout state
const unsigned long INITIAL_DELAY = 1000UL; // initial delay in ms before processing begins

void pumpSetup() {
  TCCR1B = TCCR1B & B11111000 | B00000010;    // set timer 1 (D9 & D10) divisor to 8 for PWM frequency of 3921.16 Hz (half that for 8 mhz processor?)
  pinMode(STATE_LED_PIN, OUTPUT);
  pinMode(PWM_PIN, OUTPUT);
  digitalWrite(PWM_PIN, INVERT_PWM ? HIGH : LOW);
}

void setup() {
  pumpSetup();
  oledSetup();
  if (USE_PROBE) probeSetup();
  battSetup();
  delay(INITIAL_DELAY); // delay to allow input voltage to stabilize as capacitors charge up
}

// return a lower low timeout based on the probe water level (timeout should be higher for higher water,
// because it will take longer to produce a detectable change in water level)
unsigned long getLowerLowTimeout(int probeV) {
  if (probeV > HIGH_PROBE_VOLTAGE) return LOWER_LOW_TIMEOUT; // invalid probe voltage, so no adjustment
  unsigned long res = map(probeV, STOP_VOLTAGE, HIGH_PROBE_VOLTAGE, LOWER_LOW_TIMEOUT, LOWER_LOW_TIMEOUT_MAX);
  return res;
}

// return the new pumping state
int getPumpState(int currentState, int probeV, int pot, int battEmaAdj, unsigned long timeSince, unsigned long elapsed) {
  switch(currentState) {
    case PUMPING:
      if (probeV <= STOP_VOLTAGE || battEmaAdj <= OVER_DISCHARGE && elapsed > MIN_PUMP_TIME) return STOPPED;
      if (timeSince > LOWER_LOW_TIMEOUT && timeSince > getLowerLowTimeout(probeV)) return TIMEOUT;
      break;
    case STOPPED:
      if (probeV > pot && battEmaAdj >= MIN_START_CHARGE) return PUMPING;
      break;
    case TIMEOUT:
      if (timeSince > LOWER_LOW_RECOVERY) return STOPPED;
      break;
  }
  return currentState;
}

void printDebug(int probeV, int pot, long pwm, int ema, int emaAdj, long power) {
  if (USE_PROBE) printLines(intDecStr("V", probeV), intDecStr("Pot", pot), longStr("PWM", pwm), intDecStr("Bat", ema));
  else printLines(longStr("Pwr", power/100L) + "%", intDecStr("Bat", ema), intDecStr("Adj", emaAdj), longStr("PWM", pwm));
}

/* void printDebug2(int probeV, int pot, int pwm, int maxPwm) {
  printLines(intDecStr("V", probeV), intDecStr("Pot", pot), longStr("PWM", pwm), intDecStr("Max", maxPwm));
} */

int getPower(int currentPower, bool pumping, unsigned long elapsed, int lastPower, int maxPower) {
  int power;
  if (pumping) { // pumping case
    if (elapsed < START_TIME) {
      if (currentPower < KICK_POWER) currentPower = KICK_POWER; // override the ema to ensure correct kick power
      long lastOrKick = max(lastPower, KICK_POWER);
      power = map(elapsed, 0L, START_TIME, lastOrKick, maxPower);
    }
    else power = maxPower;
  }
  else { // stop pumping or not pumping case
    if (currentPower > FULL_POWER) currentPower = FULL_POWER; // override the ema to avoid delay in decreasing output
    power = elapsed < STOP_TIME
      ? map(elapsed, 0L, STOP_TIME, lastPower, SHARP_STOP_FACTOR)
      : SHARP_STOP_FACTOR;
  }
  power = updateEma(currentPower, power);
  return constrain(power, 0, FULL_POWER);
}

int getMaxPower(int probeV, int pot, int battEmaAdj) {
  long maxByBatt = map(battEmaAdj, LOW_POWER_CHARGE, MIN_FULL_POWER_CHARGE, LOW_POWER, XFULL_POWER);
  if (!USE_PROBE || !LOW_WATER_LOW_POWER) return constrain(maxByBatt, LOW_POWER, XFULL_POWER);
  long maxRatioPoint = STOP_VOLTAGE + long(pot - STOP_VOLTAGE) * SLOW_RATIO / 100;
  long maxByProbe = map(probeV, STOP_VOLTAGE, maxRatioPoint, LOW_POWER, XFULL_POWER);
  long minMax = min(maxByBatt, maxByProbe);
  return constrain(minMax, LOW_POWER, XFULL_POWER);
}

// adjust power to compensate for the current batt voltage
long battAdjustedPower(int power, int battEma) {
  return long(power) * PUMP_VOLTAGE / battEma;
}

int normalizePwm(int power, int battEma) {
  long adjusted = battAdjustedPower(power, battEma);
  long pwm = map(adjusted, 0L, FULL_POWER, 0L, MAX_PWM);
  if (pwm > FULL_PWM) pwm = MAX_PWM;
  else if (pwm < 0L) pwm = 0L;
  return pwm/1000L;
}

void loop() {
  static int pumpingState = STOPPED;
  static unsigned long lastTime = 0UL;
  static unsigned long lastStateChange = 0UL;
  static int probeV = 0;
  static int lowProbeV = VCC;
  static unsigned long timeoutTimestamp = 0UL;
  static int pot = VCC;
  static int power = 0; // pump output power from 0 to 10000, where 10000 means run the pump PUMP_VOLTAGE voltage level
  static int lastPower = 0;
  static int normalizedPwm = 0;
  static int battV = 0;
  static int battEma = 0;
  static int battEmaAdj = 0;
  static bool pumping = false;
  static unsigned long elapsed = 0UL;
  unsigned long time = millis();
  if (time - lastTime >= SLOW_LOOP_DELAY) { // slow loop
    lastTime = time;
    probeV = getProbeV();
    // if pumping, time out from the last new low of probeV, to ensure water level is always going down
    // (otherwise pump may be failing to pump water and should be stopped)
    if (!USE_PROBE || pumping && probeV < lowProbeV) {
      lowProbeV = probeV;
      timeoutTimestamp = time;
    }
    pot = getPotSetting();
    battV = getBattV();
    battEma = getBattEma(battV, battEma);
    battEmaAdj = getBattEma(getBattVAdj(battV, power), battEmaAdj);
    unsigned long timeSince = time - timeoutTimestamp;
    int newState = getPumpState(pumpingState, probeV, pot, battEmaAdj, timeSince, elapsed);
    if (newState != pumpingState) {
      pumpingState = newState;
      pumping = pumpingState == PUMPING;
      timeoutTimestamp = lastStateChange = time;
      lastPower = power;
      lowProbeV = VCC;
    }
    if (oledConnected()) printDebug(probeV, pot, normalizedPwm, battEma, battEmaAdj, power);
  }
  // fast loop:
  elapsed = time - lastStateChange;
  int maxPower = getMaxPower(probeV, pot, battEmaAdj);
  power = getPower(power, pumping, elapsed, lastPower, maxPower);
  normalizedPwm = normalizePwm(power, battEma);
  writeStateLed(pumpingState, time);
  writePWM(normalizedPwm);
}

void writeStateLed(int pumpingState, unsigned long time) {
  switch (pumpingState) {
    case PUMPING:
      digitalWrite(STATE_LED_PIN, HIGH); // pumping state makes the built-in LED light up
      break;
    case STOPPED:
      digitalWrite(STATE_LED_PIN, LOW); // stopped makes it dark
      break;
    case TIMEOUT: // timeout state makes it flash
      digitalWrite(STATE_LED_PIN, time % TIMEOUT_FLASH_PERIOD < TIMEOUT_FLASH_DURATION ? HIGH : LOW);
      break;
  }
}

void writePWM(int normalized) {
  static int oldPwm = 0;
  if (normalized == oldPwm) return;
  analogWrite(PWM_PIN, INVERT_PWM ? MAX_ANALOG_WRITE-normalized : normalized);          // Send PWM value to pwm pin
  oldPwm = normalized;
}
